*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_manageUser}      http://127.0.0.1:8080/manageUser
${link_edit1}          xpath=//a[@href="/manageUser_edit/1"]
${password_value1}     1
${password}            password
${btnOK}               ok

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}
กดลิงค์
    [Arguments]         ${url_link}
    [Documentation]     ใช้กดลิงค์ 
    Click Link          ${url_link}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}

*** Test Case ***  
TC-PSF-02-07-01 ตรวจสอบการกรอกข้อมูลรหัสผ่าน
    เปิดหน้าจอ  ${url_manageUser}
    กดลิงค์     ${link_edit1}         
    กรอกข้อมูล  ${password}  ${password_value1}
    กดปุ่ม  ${btnOK}   

