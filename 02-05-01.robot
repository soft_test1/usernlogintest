*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_manageUser}      http://127.0.0.1:8080/manageUser
${url_addUser}         http://127.0.0.1:8080/manageUser_add
${nameUser_value1}     สมนึก เองงับ
${nameUser}            nameuser
${user_value1}         somnukkub
${user}                username
${password_value1}     123456
${password}            password
${btnOK}             ok

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}

*** Test Case ***  
TC-PSF-02-05-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ       ${url_manageUser}
    เปิดหน้าจอ       ${url_addUser}
    กรอกข้อมูล       ${nameUser}  ${nameUser_value1}
    กรอกข้อมูล       ${user}      ${user_value1}
    กรอกข้อมูล       ${password}  ${password_value1}
    กดปุ่ม           ${btnOK}

