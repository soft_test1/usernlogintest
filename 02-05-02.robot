*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_manageUser}      http://127.0.0.1:8080/manageUser
${url_addUser}         http://127.0.0.1:8080/manageUser_add
${nameUser_value1}     สมนึก เองงับ
${nameUser}            nameuser
${user_value1}         somnukkub
${user}                username
${password_value1}     123456
${password}            password
${btnCancel}           cancel

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}

*** Test Case ***  
TC-PSF-02-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ       ${url_manageUser}
    เปิดหน้าจอ       ${url_addUser}
    กดปุ่ม           ${btnCancel}

