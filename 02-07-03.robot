*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_manageUser}      http://127.0.0.1:8080/manageUser
${link_edit1}          xpath=//a[@href="/manageUser_edit/1"]
${name_value1}         ดร.พิเชษ วะยะลุน
${name}                nameuser

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดลิงค์
    [Arguments]         ${url_link}
    [Documentation]     ใช้กดลิงค์ 
    Click Link          ${url_link}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}

*** Test Case ***  
TC-PSF-02-07-01 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ  ${url_manageUser}
    กดลิงค์     ${link_edit1}         
    กรอกข้อมูล  ${name}  ${name_value1}


