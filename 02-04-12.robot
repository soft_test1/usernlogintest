*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_addUser}         http://127.0.0.1:8080/manageUser_add
${nameUser_value1}     er21$5)ยน๗xXa^
${nameUser}            nameuser

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}

*** Test Case ***  
TC-PSF-02-04-12 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุลเป็นตัวอักษรมั่ว
    เปิดหน้าจอ       ${url_addUser}
    กรอกข้อมูล       ${nameUser}  ${nameUser_value1}

