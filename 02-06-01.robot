*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_manageUser}      http://127.0.0.1:8080/manageUser
${url_addUser}         http://127.0.0.1:8080/manageUser_add
${btnAdd}              btnAdd
${select_dropdown}     statusUser
${option2}             อาจารย์
${link_add}            xpath=//a[@href="/manageUser_add"]

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}
กดลิงค์
    [Arguments]         ${url_link}
    [Documentation]     ใช้กดลิงค์ 
    Click Link          ${url_link}
เลือกตำแหน่ง
    [Arguments]         ${label}
    [Documentation]     ใช้เลือกตำแหน่ง
    Select From List By Label  ${label}

*** Test Case ***  
TC-PSF-02-06-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลตำแหน่ง
    เปิดหน้าจอ       ${url_manageUser}
    กดลิงค์          ${link_add}
    เลือกตำแหน่ง     ${select_dropdown} 

