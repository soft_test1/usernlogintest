*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_addUser}         http://127.0.0.1:8080/manageUser_add
${user_value1}         Sakda777
${text_user}           username
${password_value1}     12458
${password}            password
${option2}             อาจารย์
${btnAdd}              btnAdd
${label_option}        statusUser

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}
เลือกตําแหน่ง
    [Arguments]         ${label}    ${Select_option}
    [Documentation]     ใช้เลือกตำแหน่ง
    Select From List By Label  ${label}     ${Select_option}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}

*** Test Case ***  
TC-PSF-02-04-09 ตรวจสอบการหากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ       ${url_addUser}
    กรอกข้อมูล       ${text_user}  ${user_value1}
    กรอกข้อมูล       ${password}   ${password_value1}
    เลือกตําแหน่ง     ${label_option}  ${option2}
